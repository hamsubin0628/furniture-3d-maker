import styles from "@/styles/globals.css";

export default function Index() {
    return (
        <div>
            <div className="hsb-title-section">
                <p className="hsb-sub-title">3D PREFABRICATED FURNITURE MAKERS</p>
                <p className="hsb-title">조립식 가구 3D 메이커</p>
            </div>
            <div className="hsb-line"></div>
            <form>
            <div className="hsb-img-upload">
                    <label htmlFor="input-file" className="hsb-upload-text">
                        도면 사진을 찍어서 업로드 해주세요!
                        <div className="hsb-plus-zone">
                            <p className="hsb-plus">+</p>
                        </div>
                    </label>
                    <input className="hsb-img-input" id="input-file" type="file" style={{display: "none"}} multiple
                           accept="image/*"/>
                    <button className="hsb-btn">3D로 만들기</button>
                </div>
            </form>
        </div>
    )
}