'use client'

import { useState } from "react";
import axios from "axios";

export default function GeminiPage() {
    const [question, setQuestion] = useState('')
    const [response, setResponse] = useState('')

    const handleQuestion = e => {
        setQuestion(e.target.value)
    }

    const handleSubmit = e => {
        e.preventDefault()
        // Gemini API 콜
        const apiUrl = 'https://generativelanguage.googleapis.com/v1/models/gemini-1.5-flash:generateContent?key='

        // 데이터
        const data = { "contents":[{ "parts":[{"text": question}]}]}
        // axios : 전화기, 프로미스 객체를 구현해준 것
        axios.post(apiUrl, data)
            .then(res => {
                setResponse(res.data.candidates[0].content.parts[0].text) // 몸통
            })
            .catch(err => {
                console.log(err)
            })
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input type="text" value={question} onChange={handleQuestion} placeholder="궁금한 내용을 입력해주세요" />
                <button type="submit">확인</button>
            </form>
            {/* tip response 조건이 참이어야 {response}가 보인다 => if 문을 대신할 수 있음 */}
            {response && <p>{response}</p>}
        </div>
    )
}